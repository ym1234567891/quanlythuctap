<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Validator;
use Auth;
use Illuminate\Support\MessageBag;

use App;
use App\Http\Requests;


use Illuminate\Support\Facades\DB;
class LoginController extends Controller
{
    
    public function getLogin(Request $request) {
        //$request->session()->flush();
    	return view('login');
    }
    public function postLogin(Request $request) {
    	$rules = [
    		'email' =>'required|email',
    		'password' => 'required|min:8'
    	];
    	$messages = [
    		'email.required' => 'Email là trường bắt buộc',
    		'email.email' => 'Email không đúng định dạng',
    		'password.required' => 'Mật khẩu là trường bắt buộc',
    		'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
    	];
    	$validator = Validator::make($request->all(), $rules, $messages);

    	if ($validator->fails()) {
    		return redirect()->back()->withErrors($validator)->withInput();
    	} else {
    		$email = $request->input('email');
    		$password = $request->input('password');
            //$user = DB::table('users')->select('name')->where('email', $email )->get();
            //print_r($user[0]->name);
            //die();
            $id = DB::table('users')->where('email', $email )->value('id');
            $role = DB::table('users')->where('email', $email )->value('role_id');

            //print_r($id);
    		if( Auth::attempt(['email' => $email, 'password' =>$password])) {
                $request->session()->put('id', $id);
                $request->session()->put('role_id', $role);
               // print_r($request->session()->get('id'));
                if($role ==1) return redirect()->intended('/');
                if($role ==2) return redirect()->intended('studentinfo');
                if($role==3){
                    $type = DB::table('teachers')->where('id', $id )->value('type');
                    

                        $request->session()->put('id', $id);
                        $request->session()->put('type', $type);
                        if($type=="instructor")
                            return redirect()->intended('teachersinfo');
                        else
                            return redirect()->intended('teacherreponsible');
                    
                }
                if($role ==5) {
                    $instructor_id = DB::table('instructors')->where('id', $id )->value('instructor_id');
                    $request->session()->put('instructor_id', $instructor_id);
                    return redirect()->intended('instructorsinfo');

                }
                if($role ==4){
                    $enterprise_id = DB::table('enterprises')->where('id', $id )->value('enterprise_id');
                    $request->session()->put('enterprise_id', $enterprise_id);
                    return redirect()->intended('enterprisesinfo');
                } 
                
    			
    		} else {
    			$errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    	}
    }
}