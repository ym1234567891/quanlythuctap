<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class teachersinfoController extends Controller{
	
	public function getTeachersinfo( Request $request){
		if($request->session()->get('id')!=null){
			$users1 = DB::table('users')->where('id', $request->session()->get('id') )->get();
		
			$teacher =DB::table('teachers')->where('id', $request->session()->get('id') )->get();
			
			$topics =DB::table('topics')->get();
			$students_mng=null;
			$students_mng = DB::table('students')->get();
			if(isset($_GET['std_id'])){
				
			}
			if(isset($_GET['id'])) {

				$students =DB::table('students')->where(['topic1_id'=> $_GET['id']])

												->orwhere('topic2_id',$_GET['id'])
												->orwhere('topic3_id',$_GET['id'])->get();
				$topic_edit =DB::table('topics')->where('topic_id', $_GET['id'])->get();
				$instructors =DB::table('teachers')->where('type', "reponsible" )->get();
				$student_w = null;
				foreach ($students  as $student) {
					# code...

					$cv =DB::table('cv')->where('id', $student->id)->get();
					if($cv[0]->c==1) $cv[0]->c=0;
					if($cv[0]->java==1) $cv[0]->java=0;
					if($cv[0]->php==1) $cv[0]->php=0;
					if($cv[0]->android==1) $cv[0]->android=0;
					if($cv[0]->ios==1) $cv[0]->ios=0;

					

					$student_w[$student->id] = round(($cv[0]->c*$topic_edit[0]->c_w+$cv[0]->java*$topic_edit[0]->java_w+$cv[0]->php*$topic_edit[0]->php_w+$cv[0]->android*$topic_edit[0]->android_w+$cv [0]->ios*$topic_edit[0]->ios_w)/($topic_edit[0]->ios_w+$topic_edit[0]->c_w+$topic_edit[0]->android_w+$topic_edit[0]->php_w+$topic_edit[0]->java_w),1);
				}
				
				
				
				return view("processingtopic",['students'=>$students ,'topic_edit'=>$topic_edit,'instructors'=>$instructors,'student_w'=>$student_w]);
			}
			else
			return view("teachersInfo",['users1' => $users1,'topics' =>$topics,'teacher'=>$teacher,'students_mng'=>$students_mng]);
		}
		else{
			return redirect()->intended('login');
		}
		
		
		
	}
	public function postTeachersinfo(Request $request){
		//Studeninfo 
		if(isset($_POST['form1'])){
			$name= $request->input('name1');
			$email=$request->input('email1');
			$password=$request->input('password1');
			$phonenumber =$request->input('phonenumber1');
			
			$address=$request->input('address1');

			$update=DB::table('users')
            ->where('id', $request->session()->get('id'))
            ->update(['name' => $name],
            		 ['email' => $email],
            		 ['password'=>bcrypt($password)]
            );
            DB::enableQueryLog();
            
            $update2=DB::table('teachers')
            ->where('id', $request->session()->get('id'))
            ->update([
            		//'email' => $email,
	            	'fullname' => $name,
	            	
	            	'phonenumber' => $phonenumber,
	            	'address' => $address
            	]
            );

           // dd(DB::getQueryLog());
            if($update && $update2) return redirect()->intended('teachersinfo');
            else return redirect()->intended('teachersinfo');
		}

		if(isset($_POST['save'])){
			$name_topic= $request->input('name_topic');
			$student_number=$request->input('student_number');
			$requirements=$request->input('requirements');
			
			
			$start_time= $request->input('start_time');
			$end_time=$request->input('end_time');
			$description=$request->input('description');

			
		
			$instructors =DB::table('instructors')->where('id', $request->session()->get('id') )->get();

			$topics = DB::table('topics')->insert(
					    ['name' => $name_topic, 
					     'student_number' => $student_number,
					     'requirements'=>$requirements,
					     'start_time'=>$start_time,
					     'end_time' =>$end_time,
					     'description'=>$description,
					     
					     'instructor_id'=>$instructors[0]->instructor_id
					    ]
					);
					
			
    		
		
					
    		if($instructors){
    			
    			return redirect()->intended('teachersinfo');
    		} 
    		else{
    			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		
    	}


   //  	if(isset($_POST['edit'])){
			// $name_topic= $request->input('name_topic');
			// $student_number=$request->input('student_number');
			// $requirements=$request->input('requirements');
			
			
			// $start_time= $request->input('start_time');
			// $end_time=$request->input('end_time');
			// $description=$request->input('description');

			
		
			

			// $topics = DB::table('topics')->where('topic_id', $request->session()->get('topic_id'))->update(
			// 		    ['name' => $name_topic, 
			// 		     'student_number' => $student_number,
			// 		     'requirements'=>$requirements,
			// 		     'start_time'=>$start_time,
			// 		     'end_time' =>$end_time,
			// 		     'description'=>$description,
					     
					     
			// 		    ]
			// 		);
					
			
    		
		
					
   //  		if($topics){
    			
   //  			return redirect()->intended('instructorsinfo');
   //  		} 
   //  		else{
   //  			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
   //  			return redirect()->back()->withInput()->withErrors($errors);
   //  		}
    		
   //  	}
    	if(isset($_POST['savetopic'])){
    		
    		
    		$students_id= $request->input('students_id');
    		$instructor = $request->input('instructor');
    		//dd($students_id);
    		//dd($_POST['students_id']);
    		$topic_edit =DB::table('topics')->where('topic_id', $_GET['id'])->get();
    		//$instructors =DB::table('instructors')->where('id', $topic_edit[] )->get();
    		
    		for ($i=0;$i<count($students_id);$i++) {
    			$update2=DB::table('students')

	            ->where('id', $students_id[$i])
	            ->update([
	            		'selected_topic_id' => $_GET['id'],
	            		'enterprise_instructor_id' => $topic_edit[0]->instructor_id,
	            		'instructor_id' => $request->session()->get('id'),
	            		'responsible_id' =>$instructor[$i],
		            	
	            	]
	            );

	            $topics = DB::table('students')->where('id', $students_id[$i])->update(
					    ['topic1_id' => "", 
					     'topic2_id' => "",
					     'topic3_id'=>"",
					    
					     
					     
					    ]
					);
    		}

    		return redirect()->intended('teachersinfo');
    	}
	}

}