<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class studentRegisterController extends Controller
{
	public function getRegister(){
		return view("studentRegister");
	}
	public function postRegister(Request $request){
		$rules = [
    		'email' =>'required|email',
    		'password' => 'required|min:8',
    		'fullname' => 'required|min:8',
    		'masosv' =>'required',
    		'repassword' =>'required|same:password',
    		'class' =>'required'
    	];
    	$messages = [
    		'email.required' => 'Email là trường bắt buộc',
    		'email.email' => 'Email không đúng định dạng',
    		'password.required' => 'Mật khẩu là trường bắt buộc',
    		'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
    		'fullname.required' =>'Fullname là trường bắt buộc',
    		'masosv.required' => 'Mã số sinh viên là trường bắt buộc',
    		'repassword.required' => 'Mật khẩu là trường bắt buộc',
    		'repassword.same' => 'Re-Password Không trùng',
    		
    		
    		'class.required' => 'Class là trường bắt buộc',

    	];
    	$validator = Validator::make($request->all(), $rules, $messages);
    	if ($validator->fails()) {
    		return redirect()->back()->withErrors($validator)->withInput();

    	} else {
    		$fullname = $request->input('fullname');
    		$masosv = $request->input('masosv');
    		$class = $request->input('class');
    		$email = $request->input('email');
    		$password = $request->input('password');
    		$user = DB::table('users')->where('email', $email )->count();
    		if( $user==0){
    			$user = App\User::create([
		        	'name' => $fullname,
		        	'email' =>$email,
		        	'role_id' => 2,
		        	'password' => bcrypt($password)
	       		 ]);
    			$id = DB::table('users')->where('email', $email )->value('id');
    			$students = DB::table('students')->insert(
						    ['fullname' => $fullname, 
						     'studen_code' => $masosv,
						     'class_name'=>$class,
						     'id'=>$id,
						    ]
						);
						
				
	    		$cv = DB::table('cv')->where('id', $id )->count();
	    		print_r($cv);

			
				$insert=DB::table('cv')->insert(
				    ['id' => $id , 
				     
				    ]
				);
			
						
	    		if($user && $students){
	    			$request->session()->put('id', $id);
	    			print_r($request->session()->get('id'));
	    			return redirect()->intended('/');
	    		} 
	    		else{
	    			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
	    			return redirect()->back()->withInput()->withErrors($errors);
	    		}
    		}
    		else{
    			$errors = new MessageBag(['errorlogin' => 'Email đã được đăng ký']);
	    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		
    		
    		
    		/*if( Auth::attempt(['email' => $email, 'password' =>$password])) {
    			return redirect()->intended('/');
    		} else {
    			$errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		*/
    	}
	}
}