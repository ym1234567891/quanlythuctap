<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class logout extends Controller{
	
	public function getLogout( Request $request){
		$request->session()->flush();
		return redirect()->intended('login');
		
	}
	

}