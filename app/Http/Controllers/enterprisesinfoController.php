<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class enterprisesInfoController extends Controller{
	
	public function getEnterprisesinfo( Request $request){
		if($request->session()->get('id')!=null){
			$users1 = DB::table('users')->where('id', $request->session()->get('id') )->get();
		
		
			$enterprises =DB::table('enterprises')->where('id', $request->session()->get('id') )->get();
			$instructors =DB::table('instructors')->where('enterprise_id', $request->session()->get('enterprise_id') )->get();
			print_r(count($instructors));
			//echo $users2[0]->class_name;
			//$cv = DB::table('cv')->where('id', $request->session()->get('id') )->get();
			//$cv = DB::table('cv')->select('toeic_point','c','java','android','php','ios','skills_sytem','other_description','soft_skills','knowledge','ibm','')
			
			//if($request->session()->has('id')) return view("studentinfor",['users1' => $users1,'users2'=>$users2,'cv'=>$cv]);
			return view("enterprisesInfo",['users1' => $users1,'enterprises'=>$enterprises,'instructors'=>$instructors]);
		}else{
			return redirect()->intended('login');
		}
		
		
		
	}
	public function postEnterprisesinfo(Request $request){
		//Studeninfo 
		if(isset($_POST['form1'])){
			$name= $request->input('name1');
			$email=$request->input('email1');
			$password=$request->input('password1');
			$phonenumber =$request->input('phonenumber1');
			
			$address=$request->input('address1');

			$update=DB::table('users')
            ->where('id', $request->session()->get('id'))
            ->update(['name' => $name],
            		 ['email' => $email],
            		 ['password'=>bcrypt($password)]
            );
            DB::enableQueryLog();
            
            $update2=DB::table('enterprises')
            ->where('id', $request->session()->get('id'))
            ->update([
            		//'email' => $email,
	            	'name' => $name,
	            	
	            	'phonenumber' => $phonenumber,
	            	'address' => $address
            	]
            );

           // dd(DB::getQueryLog());
            if($update && $update2) return redirect()->intended('enterprisesinfo');
            else return redirect()->intended('enterprisesinfo');
		}

		if(isset($_POST['register'])){
			$name= $request->input('fullname');
			$email=$request->input('email');
			$password=$request->input('password');
			
			
			$phonenumber= $request->input('phonenumber');
			$address=$request->input('address');

			
		
			$user = DB::table('users')->where('email', $email )->count();
    		if( $user==0){
    			$user = App\User::create([
		        	'name' => $name,
		        	'email' =>$email,
		        	'role_id' => 5,
		        	'password' => bcrypt($password)
	       		 ]);
    			$id = DB::table('users')->where('email', $email )->value('id');
    			echo $enterprise_id = DB::table('enterprises')->where('id', $request->session()->get('id') )->value('enterprise_id');

    			$instructors = DB::table('instructors')->insert(
						    ['fullname' => $name, 
						     'phonenumber' => $phonenumber,
						     'address'=>$address,
						     'id'=>$id,
						     'email' =>$email,
						     'enterprise_id'=>$enterprise_id
						    ]
						);
						
				
	    		
			
						
	    		if($user && $instructors){
	    			
	    			return redirect()->intended('enterprisesinfo');
	    		} 
	    		else{
	    			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
	    			return redirect()->back()->withInput()->withErrors($errors);
	    		}
    		}
    		else{
    			$errors = new MessageBag(['errorlogin' => 'Email đã được đăng ký']);
	    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    	}
	}

}