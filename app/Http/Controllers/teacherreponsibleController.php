<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class teacherreponsibleController extends Controller{
	
	public function getReponsible( Request $request){
		if($request->session()->get('id')!=null){
			$users1 = DB::table('users')->where('id', $request->session()->get('id') )->get();
		
			$teacher =DB::table('teachers')->where('id', $request->session()->get('id') )->get();
			//$enterprises =DB::table('enterprises')->where('enterprise_id', $instructors[0]->enterprise_id )->get();
			$topics =DB::table('topics')->get();

			$student_mng= DB::table('students')->where('responsible_id',$request->session()->get('id'))->get();
			for($i=0;$i<count($student_mng);$i++){
				$topic_mng[$i]=DB::table('topics')->where('topic_id', $student_mng[$i]->selected_topic_id)->get();
			}

			//echo $users2[0]->class_name;
			//$cv = DB::table('cv')->where('id', $request->session()->get('id') )->get();
			//$cv = DB::table('cv')->select('toeic_point','c','java','android','php','ios','skills_sytem','other_description','soft_skills','knowledge','ibm','')
			
			//if($request->session()->has('id')) return view("studentinfor",['users1' => $users1,'users2'=>$users2,'cv'=>$cv]);
			if(isset($_GET['id'])) {

				$students =DB::table('students')->where(['topic1_id'=> $_GET['id']])

												->orwhere('topic2_id',$_GET['id'])
												->orwhere('topic3_id',$_GET['id'])->get();
				$topic_edit =DB::table('topics')->where('topic_id', $_GET['id'])->get();
				$instructors =DB::table('teachers')->where('type', "reponsible" )->get();
				$student_w=null;
				foreach ($students  as $student) {
					# code...

					$cv =DB::table('cv')->where('id', $student->id)->get();
					if($cv[0]->c==1) $cv[0]->c=0;
					if($cv[0]->java==1) $cv[0]->java=0;
					if($cv[0]->php==1) $cv[0]->php=0;
					if($cv[0]->android==1) $cv[0]->android=0;
					if($cv[0]->ios==1) $cv[0]->ios=0;

					

					$student_w[$student->id] = round(($cv[0]->c*$topic_edit[0]->c_w+$cv[0]->java*$topic_edit[0]->java_w+$cv[0]->php*$topic_edit[0]->php_w+$cv[0]->android*$topic_edit[0]->android_w+$cv [0]->ios*$topic_edit[0]->ios_w)/($topic_edit[0]->ios_w+$topic_edit[0]->c_w+$topic_edit[0]->android_w+$topic_edit[0]->php_w+$topic_edit[0]->java_w),1);
				}
				
				
				//$request->session()->put('topic_id', $_GET['id']);
				return view("processingtopic",['students'=>$students ,'topic_edit'=>$topic_edit,'instructors'=>$instructors,'student_w'=>$student_w]);
			}
			else
			return view("teacherreponsible",['users1' => $users1,'topics' =>$topics,'teacher'=>$teacher,'student_mng'=>$student_mng,'topic_mng'=>$topic_mng]);
		}
		else{
			return redirect()->intended('login');
		}
		
		
		
	}
	public function postReponsible(Request $request){
		//Studeninfo 
		if(isset($_POST['form1'])){
			$name= $request->input('name1');
			$email=$request->input('email1');
			$password=$request->input('password1');
			$phonenumber =$request->input('phonenumber1');
			
			$address=$request->input('address1');

			$update=DB::table('users')
            ->where('id', $request->session()->get('id'))
            ->update(['name' => $name],
            		 ['email' => $email],
            		 ['password'=>bcrypt($password)]
            );
            DB::enableQueryLog();
            
            $update2=DB::table('instructors')
            ->where('id', $request->session()->get('id'))
            ->update([
            		'email' => $email,
	            	'fullname' => $name,
	            	
	            	'phonenumber' => $phonenumber,
	            	'address' => $address
            	]
            );

           // dd(DB::getQueryLog());
            if($update && $update2) return redirect()->intended('teacherreponsible');
            else return redirect()->intended('teacherreponsible');
		}

		
		if(isset($_POST['form2'])){
			$midterm_score= $request->input('midterm_score');
			$endterm_score=$request->input('endterm_score');
			foreach ($midterm_score as $key => $value) {
				# code...
				$update=DB::table('students')
	            ->where('studen_code', $key)
	            ->update(['midterm_score' => $value]
	            		 
	            );
			}
			foreach ($endterm_score as $key => $value) {
				# code...
				$update=DB::table('students')
	            ->where('studen_code', $key)
	            ->update(['endterm_score' => $value]
	            		 
	            );
			}

			
            DB::enableQueryLog();
            return redirect()->intended('teacherreponsible');
            
		}

   //  	if(isset($_POST['edit'])){
			// $name_topic= $request->input('name_topic');
			// $student_number=$request->input('student_number');
			// $requirements=$request->input('requirements');
			
			
			// $start_time= $request->input('start_time');
			// $end_time=$request->input('end_time');
			// $description=$request->input('description');

			
		
			

			// $topics = DB::table('topics')->where('topic_id', $request->session()->get('topic_id'))->update(
			// 		    ['name' => $name_topic, 
			// 		     'student_number' => $student_number,
			// 		     'requirements'=>$requirements,
			// 		     'start_time'=>$start_time,
			// 		     'end_time' =>$end_time,
			// 		     'description'=>$description,
					     
					     
			// 		    ]
			// 		);
					
			
    		
		
					
   //  		if($topics){
    			
   //  			return redirect()->intended('instructorsinfo');
   //  		} 
   //  		else{
   //  			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
   //  			return redirect()->back()->withInput()->withErrors($errors);
   //  		}
    		
   //  	}
    	if(isset($_POST['savetopic'])){
    		
    		
    		$students_id= $request->input('students_id');
    		$instructor = $request->input('instructor');
    		//dd($students_id);
    		//dd($_POST['students_id']);
    		$topic_edit =DB::table('topics')->where('topic_id', $_GET['id'])->get();
    		//$instructors =DB::table('instructors')->where('id', $topic_edit[] )->get();
    		
    		for ($i=0;$i<count($students_id);$i++) {
    			$update2=DB::table('students')

	            ->where('id', $students_id[$i])
	            ->update([
	            		'selected_topic_id' => $_GET['id'],
	            		'enterprise_instructor_id' => $topic_edit[0]->instructor_id,
	            		'instructor_id' => $request->session()->get('id'),
	            		'responsible_id' =>$instructor[$i],
		            	
	            	]
	            );

	            $topics = DB::table('students')->where('id', $students_id[$i])->update(
					    ['topic1_id' => "", 
					     'topic2_id' => "",
					     'topic3_id'=>"",
					    
					     
					     
					    ]
					);
    		}

    		return redirect()->intended('teachersinfo');
    	}
	}

}