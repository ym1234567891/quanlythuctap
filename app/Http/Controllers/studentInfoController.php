<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class studentInfoController extends Controller{
	
	public function getStudentinfo( Request $request){
		if($request->session()->get('id')!=null)
		if(!isset($_GET['id'])){

			$users1 = DB::table('users')->select('name','email','password')->where('id', $request->session()->get('id') )->get();
			//echo $users1[0]->name;
			
			$users2 =DB::table('students')->where('id', $request->session()->get('id') )->get();
			//echo $users2[0]->class_name;
			$cv = DB::table('cv')->where('id', $request->session()->get('id') )->get();
			$topics = DB::table('topics')->get();
			$teachers= null;

			$teachers =DB::table('teachers')->where('id', $users2[0]->responsible_id )->get();
///////////////////////////

			$student_w = null;
			foreach ($topics  as $topic) {
				# code...

				//$cv =DB::table('cv')->where('id', $request->session()->get('id'))->get();
				if($cv[0]->c==1) $cv[0]->c=0;
				if($cv[0]->java==1) $cv[0]->java=0;
				if($cv[0]->php==1) $cv[0]->php=0;
				if($cv[0]->android==1) $cv[0]->android=0;
				if($cv[0]->ios==1) $cv[0]->ios=0;

				

				$student_w[$topic->topic_id] = round(($cv[0]->c*$topic->c_w+$cv[0]->java*$topic->java_w+$cv[0]->php*$topic->php_w+$cv[0]->android*$topic->android_w+$cv [0]->ios*$topic->ios_w)/($topic->ios_w+$topic->c_w+$topic->android_w+$topic->php_w+$topic->java_w),1);
			}
///////////////////////////////			
			//dd($teachers);
			//$cv = DB::table('cv')->select('toeic_point','c','java','android','php','ios','skills_sytem','other_description','soft_skills','knowledge','ibm','')
			if($users2[0]->selected_topic_id!=""){
				$topic_select = DB::table('topics')->where('topic_id', $users2[0]->selected_topic_id )->get();

				$instructor = DB::table('instructors')->where('id', $topic_select[0]->instructor_id )->get();

				if($request->session()->has('id')) return view("studentinfor",['users1' => $users1,'users2'=>$users2,'cv'=>$cv,'topics'=>$topics,
					'topic_select'=>$topic_select,'instructor'=>$instructor,'teachers'=>$teachers,'student_w'=>$student_w]);
				else return redirect()->intended('/');
		}else{
			return view("studentinfor",['users1' => $users1,'users2'=>$users2,'cv'=>$cv,'topics'=>$topics,'teachers'=>$teachers,'student_w' =>$student_w
					]);
		}
			
		}else{
			if(isset($_GET['id'])&&isset($_GET['topic'])){
				$topic_selected = DB::table('students')->select('selected_topic_id')->where('id', $request->session()->get('id') )->get();
				if($topic_selected[0]->selected_topic_id==null){
					$topic_id =DB::table('students')->select('topic1_id','topic2_id','topic3_id')->where('id', $request->session()->get('id') )->get();
					$topic1_id=$topic_id[0]->topic1_id;
					$topic2_id=$topic_id[0]->topic2_id;
					$topic3_id=$topic_id[0]->topic3_id;

					

					$check_tp = array("topic1_id"=>$topic1_id, "topic2_id"=>$topic2_id, "topic3_id"=>$topic3_id);

					foreach ($check_tp as $key => $value){
					    if($key!=$_GET['topic'] && $value==$_GET['id']){
					    	$update1=DB::table('students')
							->where('id', $request->session()->get('id'))
							->update(
							    [
							    	$key=>null,
							    ]
							);

					    }

					   
					}
					
					$update=DB::table('students')
					->where('id', $request->session()->get('id'))
					->update(
					    [
					    	$_GET['topic']=>$_GET['id'],
					    ]
					);
					
				}
				else{
					return redirect()->intended('studentinfo');
				}
				
			}
		}
		else{
			return redirect()->intended('login');
		}
		
		
	}
	public function postStudentinfo(Request $request){
		//Studeninfo 
		DB::enableQueryLog();
		if(isset($_POST['form1'])){
			$name= $request->input('fullname');
			$email=$request->input('email');
			$password=$request->input('password');
			$class =$request->input('class_name');
			$studentcode =$request->input('studentid');
			$phonenumber= $request->input('phonenumber');
			$address=$request->input('address');

			$update=DB::table('users')
            ->where('id', $request->session()->get('id'))
            ->update(['name' => $name],
            		 
            		 ['password'=>bcrypt($password)]
            );
            DB::enableQueryLog();
            
            $update2=DB::table('students')
            ->where('id', $request->session()->get('id'))
            ->update([
	            	'fullname' => $name,
	            	'class_name' => $class,
	            	'studen_code' => $studentcode,
	            	'phonenumber' => $phonenumber,
	            	'address' => $address
            	]
            );

           // dd(DB::getQueryLog());
            if($update && $update2) return redirect()->intended('studentinfo');
            else return redirect()->intended('studentinfo');
		}
		//else die('1');
		///cv
		if(isset($_POST['save'])){
			$toeic_point= $request->input('toeic_point');
			$c= $request->input('c');
			$java= $request->input('java');
			$android= $request->input('android');
			$php= $request->input('php');
			$ios= $request->input('ios');
			$skills_system= $request->input('skills_system');
			
			$other_description= $request->input('other_description');
			$soft_skills= $request->input('soft_skills');
			$knowledges= $request->input('knowledge');
			$ibm= $request->input('ibm');
			$microsoft= $request->input('microsoft');
			$cisco= $request->input('cisco');
			$oracle= $request->input('oracle');
			$users2 =DB::table('students')->where('id', $request->session()->get('id') )->get();

			$midterm_report= $request->file('midterm_report');
    		$endterm_report= $request->file('endterm_report');

    		if($midterm_report!=null){
    			$extension = $midterm_report->getClientOriginalExtension(); // getting image extension
     		 	$fileName = $users2[0]->studen_code.'_midterm_report.'.$extension; // r
    			$midterm_report->move('fileupload/upload/',$fileName);
    			$update2=DB::table('students')
	            ->where('studen_code', $users2[0]->studen_code)
	            ->update([
	            		'midterm_report' => $extension,
		            	
	            	]
	            );
    		}
    		if($endterm_report!=null){
    			$extension = $endterm_report->getClientOriginalExtension(); // getting image extension
     		 	$fileName = $users2[0]->studen_code.'_endterm_report.'.$extension; // r
    			$endterm_report->move('fileupload/upload/',$fileName);
    			$update2=DB::table('students')
	            ->where('studen_code', $users2[0]->studen_code)
	            ->update([
	            		'endterm_report' => $extension,
		            	
	            	]
	            );
    		}
    		
				
    		

			$update=DB::table('cv')
			->where('id', $request->session()->get('id'))
			->update(
			    [
			     'toeic_point' => $toeic_point,
			     'c'=>$c,
			     'java' => $java,
			     'android' =>$android,
			     'php' => $php,
			     'ios' =>$ios,
			     'skills_system' => $skills_system,
			     'other_description' =>$other_description,
			     'soft_skills' => $soft_skills,
			     'knowledge' =>$knowledges,
			     'ibm' => $ibm,
			     'microsoft' =>$microsoft,
			     'cisco' => $cisco,
			     'oracle' => $oracle
			    ]
			);

			//dd(DB::getQueryLog());

			if($update) return redirect()->intended('studentinfo');
            else return redirect()->intended('studentinfo');
		}

	}

}