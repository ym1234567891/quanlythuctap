<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;
class enterprisesRegisterController extends Controller{
	public function getRegister(){
		return view('enterprisesRegister');
	}
	public function postRegister(Request $request){
		$rules = [
    		'email' =>'required|email',
    		'password' => 'required|min:8',
    		'name' => 'required|min:8',
    		
    		'repassword' =>'required|same:password',
            'phonenumber' =>'required',
    		'address' =>'required',
    		'description' =>'required'
    	];
    	$messages = [
    		'email.required' => 'Email là trường bắt buộc',
    		'email.email' => 'Email không đúng định dạng',
    		'password.required' => 'Mật khẩu là trường bắt buộc',
    		'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
    		'name.required' =>'Name là trường bắt buộc',
            'description.required' => 'Description là trường bắt buộc',
    		'address.required' => 'Address là trường bắt buộc',
    		'repassword.required' => 'Mật khẩu là trường bắt buộc',
    		'repassword.same' => 'Re-Password Không trùng',
    		
    		
    		'phonenumber.required' => 'Phonenumber là trường bắt buộc',

    	];
    	$validator = Validator::make($request->all(), $rules, $messages);
    	if ($validator->fails()) {
    		return redirect()->back()->withErrors($validator)->withInput();

    	} else {
    		$name = $request->input('name');
            $phonenumber = $request->input('phonenumber');
    		$address = $request->input('address');
    		$description = $request->input('description');
    		$email = $request->input('email');
    		$password = $request->input('password');
    		
    		$user = DB::table('users')->where('email', $email )->count();
    		if( $user==0){
    			$user = App\User::create([
		        	'name' => $name,
		        	'email' =>$email,
		        	'role_id' => 4,
		        	'password' => bcrypt($password)
	       		 ]);
    			$id = DB::table('users')->where('email', $email )->value('id');
                $role = DB::table('users')->where('email', $email )->value('role_id');
                
    			$enterprises = DB::table('enterprises')->insert(
						    ['name' => $name, 
                             'phonenumber' =>$phonenumber,
						     'address' =>$address,
						     'description' =>$description,
						     'id'=>$id
						    ]
						);
						
				$enterprise_id=DB::table('enterprises')->where('id', $id  )->value('enterprise_id');
	    		/*$cv = DB::table('cv')->where('id', $request->session()->get('id') )->count();
	    		print_r($cv);

			
				$insert=DB::table('cv')->insert(
				    ['id' => $request->session()->get('id'), 
				     
				    ]
				);
				*/
						
	    		if($user && $enterprises){
	    			$request->session()->put('id', $id);
                    $request->session()->put('role', $id);
                    $request->session()->put('enterprise_id', $enterprise_id);
	    			print_r($request->session()->get('id'));
	    			return redirect()->intended('/enterprisesinfo');
	    		} 
	    		else{
	    			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
	    			return redirect()->back()->withInput()->withErrors($errors);
	    		}
    		}
    		else{
    			$errors = new MessageBag(['errorlogin' => 'Email đã được đăng ký']);
	    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		
    		
    		
    		/*if( Auth::attempt(['email' => $email, 'password' =>$password])) {
    			return redirect()->intended('/');
    		} else {
    			$errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		*/
    	}
	}
}