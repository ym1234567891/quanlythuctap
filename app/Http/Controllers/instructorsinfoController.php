<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class instructorsinfoController extends Controller{
	
	public function getInstructorsinfo( Request $request){

		if($request->session()->get('id')!=null){
			$students_mng=null;
			$users1 = DB::table('users')->where('id', $request->session()->get('id') )->get();
			
			$instructors =DB::table('instructors')->where('id', $request->session()->get('id') )->get();
			$enterprises =DB::table('enterprises')->where('enterprise_id', $instructors[0]->enterprise_id )->get();
			$topics =DB::table('topics')->where('instructor_id',  $instructors[0]->id )->get();
			$students_mng =DB::table('students')->where('enterprise_instructor_id',  $request->session()->get('id') )->get();
			$sum_student=null;
			foreach ($topics as $topic)
			{
			    $sum_student[$topic->topic_id] = count(DB::table('students')->where('selected_topic_id',  $topic->topic_id )->get());
			}
			//echo $users2[0]->class_name;
			//$cv = DB::table('cv')->where('id', $request->session()->get('id') )->get();
			//$cv = DB::table('cv')->select('toeic_point','c','java','android','php','ios','skills_sytem','other_description','soft_skills','knowledge','ibm','')
			//print_r($sum_student);
			//if($request->session()->has('id')) return view("studentinfor",['users1' => $users1,'users2'=>$users2,'cv'=>$cv]);
			if(isset($_GET['id'])) {
				$topic_edit =DB::table('topics')->where('topic_id', $_GET['id'])->get();
				$request->session()->put('topic_id', $_GET['id']);
				return view("editTopic",['topic_edit'=>$topic_edit ]);
			}
			else
			return view("instructorsInfo",['users1' => $users1,'enterprises'=>$enterprises,'instructors'=>$instructors,'topics' =>$topics,'sum_student'=>$sum_student,'student_mng'=>$students_mng]);
		}
		else{
			return redirect()->intended('login');
		}
	}
	public function postInstructorsinfo(Request $request){
		//Studeninfo 
		if(isset($_POST['form1'])){
			$name= $request->input('name1');
			$email=$request->input('email1');
			$password=$request->input('password1');
			$phonenumber =$request->input('phonenumber1');
			
			$address=$request->input('address1');

			$update=DB::table('users')
            ->where('id', $request->session()->get('id'))
            ->update(['name' => $name],
            		 ['email' => $email],
            		 ['password'=>bcrypt($password)]
            );
            DB::enableQueryLog();
            
            $update2=DB::table('instructors')
            ->where('id', $request->session()->get('id'))
            ->update([
            		'email' => $email,
	            	'fullname' => $name,
	            	
	            	'phonenumber' => $phonenumber,
	            	'address' => $address
            	]
            );

           // dd(DB::getQueryLog());
            if($update && $update2) return redirect()->intended('instructorsinfo');
            else return redirect()->intended('instructorsinfo');
		}

		if(isset($_POST['save'])){
			$name_topic= $request->input('name_topic');
			$student_number=$request->input('student_number');
			//$requirements=$request->input('requirements');
			$c= $request->input('c');
			$php= $request->input('php');
			$android= $request->input('android');
			$java= $request->input('java');
			$ios= $request->input('ios');

			$c_w= $request->input('c_w');
			$php_w= $request->input('php_w');
			$android_w= $request->input('android_w');
			$java_w= $request->input('java_w');
			$ios_w= $request->input('ios_w');


			
			$start_time= $request->input('start_time');
			$end_time=$request->input('end_time');
			$description=$request->input('description');

			$w=round(((($c*$c_w)+($php*$php_w)+($android*$android_w)+($java*$java_w)+($ios*$ios_w))/($ios_w+$java_w+$android_w+$php_w+$c_w)),1);
		
			$instructors =DB::table('instructors')->where('id', $request->session()->get('id') )->get();

			$topics = DB::table('topics')->insert(
					    ['name' => $name_topic, 
					     'student_number' => $student_number,
					     //'requirements'=>$requirements,
					     'start_time'=>$start_time,
					     'end_time' =>$end_time,
					     'description'=>$description,
					     
					     'instructor_id'=>$instructors[0]->id,
					     'c' =>$c,
					     'php' =>$php,
					     'android' =>$android,
					     'java' =>$java,
					     'ios' =>$ios,

					     'c_w' =>$c_w,
					     'php_w' =>$php_w,
					     'android_w' =>$android_w,
					     'java_w' =>$java_w,
					     'ios_w' =>$ios_w,
					     'w'=>$w

					    ]
					);
					
			
    		
		
					
    		if($instructors){
    			
    			return redirect()->intended('instructorsinfo');
    		} 
    		else{
    			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		
    	}


    	if(isset($_POST['edit'])){
			$name_topic= $request->input('name_topic');
			$student_number=$request->input('student_number');
			$c= $request->input('c');
			$php= $request->input('php');
			$android= $request->input('android');
			$java= $request->input('java');
			$ios= $request->input('ios');

			$c_w= $request->input('c_w');
			$php_w= $request->input('php_w');
			$android_w= $request->input('android_w');
			$java_w= $request->input('java_w');
			$ios_w= $request->input('ios_w');


			
			$start_time= $request->input('start_time');
			$end_time=$request->input('end_time');
			$description=$request->input('description');

			$w=round(((($c*$c_w)+($php*$php_w)+($android*$android_w)+($java*$java_w)+($ios*$ios_w))/($ios_w+$java_w+$android_w+$php_w+$c_w)),1);

			
		
			

			$topics = DB::table('topics')->where('topic_id', $request->session()->get('topic_id'))->update(
					    ['name' => $name_topic, 
					     'student_number' => $student_number,
					     
					     'start_time'=>$start_time,
					     'end_time' =>$end_time,
					     'description'=>$description,
					     'c' =>$c,
					     'php' =>$php,
					     'android' =>$android,
					     'java' =>$java,
					     'ios' =>$ios,

					     'c_w' =>$c_w,
					     'php_w' =>$php_w,
					     'android_w' =>$android_w,
					     'java_w' =>$java_w,
					     'ios_w' =>$ios_w,
					     'w'=>$w
					     
					     
					    ]
					);
					
			
    		
		
					
    		if($topics){
    			
    			return redirect()->intended('instructorsinfo');
    		} 
    		else{
    			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		
    	}

    	if(isset($_POST['save1'])){
    		$timesheet= $request->file('timesheet');
    		$report= $request->file('report');
    		if($timesheet!=null)
    		foreach ($timesheet as $key => $value) {
    			var_dump($value);
				$extension = $value->getClientOriginalExtension(); // getting image extension
     		 	$fileName = $key.'_timesheet.'.$extension; // r
    			$value->move('fileupload/upload/',$fileName);
    			$update2=DB::table('students')
	            ->where('studen_code', $key)
	            ->update([
	            		'timesheet' => $extension,
		            	
	            	]
	            );
    		}
    		if($report!=null)
    		foreach ($report as $key => $value) {
    			var_dump($value);
				$extension = $value->getClientOriginalExtension(); // getting image extension
     		 	$fileName = $key.'_report.'.$extension; // r
    			$value->move('fileupload/upload/',$fileName);
    			$update2=DB::table('students')
	            ->where('studen_code', $key)
	            ->update([
	            		'report_company' => $extension,
		            	
	            	]
	            );
    		}
    	}
	}

}