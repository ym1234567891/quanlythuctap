<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;
class teacherRegisterController extends Controller{
	public function getRegister(){
		return view('teacherRegister');
	}
	public function postRegister(Request $request){
		$rules = [
    		'email' =>'required|email',
    		'password' => 'required|min:8',
    		'fullname' => 'required|min:8',
    		
    		'repassword' =>'required|same:password',
    		'phonenumber' =>'required',
    		'address' =>'required'
    	];
    	$messages = [
    		'email.required' => 'Email là trường bắt buộc',
    		'email.email' => 'Email không đúng định dạng',
    		'password.required' => 'Mật khẩu là trường bắt buộc',
    		'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
    		'fullname.required' =>'Fullname là trường bắt buộc',
    		'address.required' => 'Address là trường bắt buộc',
    		'repassword.required' => 'Mật khẩu là trường bắt buộc',
    		'repassword.same' => 'Re-Password Không trùng',
    		
    		
    		'phonenumber.required' => 'Phonenumber là trường bắt buộc',

    	];
    	$validator = Validator::make($request->all(), $rules, $messages);
    	if ($validator->fails()) {
    		return redirect()->back()->withErrors($validator)->withInput();

    	} else {
    		$fullname = $request->input('fullname');
    		$phonenumber = $request->input('phonenumber');
    		$address = $request->input('address');
    		$email = $request->input('email');
    		$password = $request->input('password');
    		$type = $request->input('type');
    		$user = DB::table('users')->where('email', $email )->count();
    		if( $user==0){
    			$user = App\User::create([
		        	'name' => $fullname,
		        	'email' =>$email,
		        	'role_id' => 3,
		        	'password' => bcrypt($password)
	       		 ]);
    			$id = DB::table('users')->where('email', $email )->value('id');
    			$teachers = DB::table('teachers')->insert(
						    ['fullname' => $fullname, 
						     'phonenumber' => $phonenumber,
						     'address'=>$address,
						     'type'=>$type,
						     'id'=>$id
						    ]
						);
						
				
	    		/*$cv = DB::table('cv')->where('id', $request->session()->get('id') )->count();
	    		print_r($cv);

			
				$insert=DB::table('cv')->insert(
				    ['id' => $request->session()->get('id'), 
				     
				    ]
				);
				*/
						
	    		if($user && $teachers){
	    			$request->session()->put('id', $id);
	    			print_r($request->session()->get('id'));
	    			return redirect()->intended('/');
	    		} 
	    		else{
	    			$errors = new MessageBag(['errorlogin' => 'Lỗi Database']);
	    			return redirect()->back()->withInput()->withErrors($errors);
	    		}
    		}
    		else{
    			$errors = new MessageBag(['errorlogin' => 'Email đã được đăng ký']);
	    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		
    		
    		
    		/*if( Auth::attempt(['email' => $email, 'password' =>$password])) {
    			return redirect()->intended('/');
    		} else {
    			$errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
    			return redirect()->back()->withInput()->withErrors($errors);
    		}
    		*/
    	}
	}
}