<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('role_id');
            $table->string('name', 250)->unique();
            $table->timestamps();
        });
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('role_id')->unsigned();
            $table->rememberToken();
            $table->timestamps('created_at');
        });
        Schema::table('users', function($table) {
            $table->foreign('role_id')->references('role_id')->on('roles');
        });

        Schema::create('teachers', function($table) {
            $table->increments('teacher_id');
            $table->integer('id')->unsigned();
            $table->string('fullname', 250);
            
            $table->string('phonenumber', 20);
            $table->string('address', 250);
            $table->enum('type',['instructor','reponsible']);
            $table->timestamps();
        });

        Schema::table('teachers', function($table) {
            $table->foreign('id')->references('id')->on('users');
        });

        Schema::create('enterprises', function (Blueprint $table) {
            $table->increments('enterprise_id');
            
            $table->integer('id')->unsigned();
            $table->string('name', 250)->unique();
            $table->string('phonenumber', 250);
            $table->string('description',250);
            $table->string('address',250);

            
            $table->timestamps();
        });

        Schema::create('topics', function (Blueprint $table) {
            $table->increments('topic_id');
            
            $table->string('name', 250)->unique();
            $table->integer('student_number');
            $table->integer('instructor_id');
            $table->enum('c',['1','2','3','4']);
            $table->enum('java',['1','2','3','4']);
            $table->enum('android',['1','2','3','4']);
            $table->enum('php',['1','2','3','4']);
            $table->enum('ios',['1','2','3','4']);
            $table->integer('c_w');
            $table->integer('java_w');
            $table->integer('android_w');
            $table->integer('php_w');
            $table->integer('ios_w');
            $table->double('w');
            
            //$table->string('requirements');
            $table->date('start_time');
            $table->date('end_time');
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('instructors', function($table) {
            $table->increments('instructor_id');
            $table->integer('id')->unsigned();
            $table->integer('enterprise_id')->unsigned();
            $table->integer('topic_id')->unsigned();
            $table->string('fullname', 250);
            $table->string('email', 250);
            $table->string('phonenumber', 20);
            $table->string('address', 250);
            $table->timestamps();
        });

        Schema::table('instructors', function($table) {
            $table->foreign('id')->references('id')->on('users');
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprises');
           // $table->foreign('topic_id')->references('topic_id')->on('topics');
        });

        Schema::create('students', function($table) {
            $table->increments('student_id');
            $table->integer('id')->unsigned();
            $table->integer('enterprise_instructor_id')->unsigned();
            $table->integer('instructor_id')->unsigned();
            $table->integer('responsible_id')->unsigned();
            $table->integer('selected_topic_id')->unsigned();
            $table->string('topic1_id', 250);
            $table->string('topic2_id', 250);
            $table->string('topic3_id', 250);
            $table->string('fullname', 250);
            $table->string('timesheet', 250);
            $table->string('report_company', 250);

            //$table->string('lastname', 250);
            $table->string('phonenumber', 20);
            $table->string('class_name', 250);
            $table->string('studen_code', 250);
            $table->string('address', 250);
            $table->string('specialized', 250);
            $table->string('school_name', 250);
            $table->string('state', 250);
            $table->string('midterm_report', 250);
            $table->string('endterm_report', 250);
            $table->double('midterm_score');
            $table->double('endterm_score');
            $table->date('finished_date');
            $table->timestamps();
        });

        Schema::table('students', function($table) {
            $table->foreign('id')->references('id')->on('users');
            //$table->foreign('enterprise_instructor_id')->references('instructor_id')->on('instructors');
            //$table->foreign('instructor_id')->references('teacher_id')->on('teachers');
            //$table->foreign('responsible_id')->references('teacher_id')->on('teachers');
            //$table->foreign('selected_topic_id')->references('topic_id')->on('topics');
        });

        Schema::create('cv',function($table){
            $table->increments('cv_id');
            $table->integer('id')->unsigned();
            $table->integer('toeic_point');
            $table->enum('c',['1','2','3','4']);
            $table->enum('java',['1','2','3','4']);
            $table->enum('android',['1','2','3','4']);
            $table->enum('php',['1','2','3','4']);
            $table->enum('ios',['1','2','3','4']);

            //$table->string('lastname', 250);
            $table->string('skills_system', 250);
            $table->enum('ibm', ['false','true']);
            $table->enum('microsoft', ['false','true']);
            $table->enum('cisco', ['false','true']);
            $table->enum('oracle', ['false','true']);
            $table->enum('other', ['false','true']);
            $table->string('other_description', 250);
            $table->string('soft_skills', 250);
            $table->string('knowledge', 250);
            $table->timestamps();
        });
        Schema::table('cv', function($table) {
            //$table->foreign('id')->references('id')->on('users');
            //$table->foreign('enterprise_instructor_id')->references('instructor_id')->on('instructors');
            //$table->foreign('instructor_id')->references('teacher_id')->on('teachers');
            //$table->foreign('responsible_id')->references('teacher_id')->on('teachers');
            //$table->foreign('selected_topic_id')->references('topic_id')->on('topics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('topics');
        Schema::dropIfExists('students');
        Schema::dropIfExists('instructors');
        Schema::dropIfExists('enterprises');
        Schema::dropIfExists('teachers');
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
