<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('roles')->truncate();
        App\Roles::create([
        	'name' => 'Administrator'
        ]);
        App\Roles::create([
        	'name' => 'Student'
        ]);
        App\Roles::create([
        	'name' => 'Teacher'
        ]);
        App\Roles::create([
        	'name' => 'Enterprise'
        ]);
        App\Roles::create([
        	'name' => 'Instructor'
        ]);
        //DB::table('users')->truncate();
        App\User::create([
        	'name' => 'admin',
        	'email' =>'ym1234567891@gmail.com',
        	'role_id' => 1,
        	'password' => bcrypt('0947501966')
        ]);
    }
}
