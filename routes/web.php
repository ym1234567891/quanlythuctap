<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');

Route::get('login','LoginController@getLogin');
Route::post('login','LoginController@postLogin');

Route::get('registerstudent','studentRegisterController@getRegister');
Route::post('registerstudent','studentRegisterController@postRegister');

Route::get('registerteacher','teacherRegisterController@getRegister');
Route::post('registerteacher','teacherRegisterController@postRegister');

Route::get('studentinfo','studentInfoController@getStudentinfo');
Route::post('studentinfo','studentInfoController@postStudentinfo');

Route::get('registerenterprises','enterprisesRegisterController@getRegister');
Route::post('registerenterprises','enterprisesRegisterController@postRegister');

Route::get('enterprisesinfo','enterprisesinfoController@getEnterprisesinfo');
Route::post('enterprisesinfo','enterprisesinfoController@postEnterprisesinfo');

Route::get('instructorsinfo','instructorsinfoController@getInstructorsinfo');
Route::post('instructorsinfo','instructorsinfoController@postInstructorsinfo');
/////////
Route::get('teachersinfo','teachersinfoController@getTeachersinfo');
Route::post('teachersinfo','teachersinfoController@postTeachersinfo');

Route::get('teachersinfo','teachersinfoController@getTeachersinfo');
Route::post('teachersinfo','teachersinfoController@postTeachersinfo');


Route::get('teacherreponsible','teacherreponsibleController@getReponsible');
Route::post('teacherreponsible','teacherreponsibleController@postReponsible');

Route::get('logout','logout@getLogout');



