-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2017 at 06:55 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project2db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cv`
--

CREATE TABLE `cv` (
  `cv_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `toeic_point` int(11) NOT NULL,
  `c` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `java` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `android` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `php` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `ios` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `skills_system` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `ibm` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `microsoft` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `cisco` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `oracle` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `other` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `other_description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `soft_skills` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `knowledge` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `cv`
--

INSERT INTO `cv` (`cv_id`, `id`, `toeic_point`, `c`, `java`, `android`, `php`, `ios`, `skills_system`, `ibm`, `microsoft`, `cisco`, `oracle`, `other`, `other_description`, `soft_skills`, `knowledge`, `created_at`, `updated_at`) VALUES
(1, 4, 0, '3', '3', '3', '2', '2', '', 'true', 'true', 'true', 'true', 'false', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `enterprises`
--

CREATE TABLE `enterprises` (
  `enterprise_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phonenumber` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `enterprises`
--

INSERT INTO `enterprises` (`enterprise_id`, `id`, `name`, `phonenumber`, `description`, `address`, `created_at`, `updated_at`) VALUES
(1, 2, 'NetBit solution', '0123658547', 's', 'hanoi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE `instructors` (
  `instructor_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `enterprise_id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phonenumber` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `instructors`
--

INSERT INTO `instructors` (`instructor_id`, `id`, `enterprise_id`, `topic_id`, `fullname`, `email`, `phonenumber`, `address`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 0, 'Nguyễn Ngọc Phương', 'nguyenngocphuong@gmail.com', '0455502223', 'hanoi, ninh binh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2017-05-18 00:13:55', '2017-05-18 00:13:55'),
(2, 'Student', '2017-05-18 00:13:55', '2017-05-18 00:13:55'),
(3, 'Teacher', '2017-05-18 00:13:55', '2017-05-18 00:13:55'),
(4, 'Enterprise', '2017-05-18 00:13:55', '2017-05-18 00:13:55'),
(5, 'Instructor', '2017-05-18 00:13:55', '2017-05-18 00:13:55');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `enterprise_instructor_id` int(10) UNSIGNED NOT NULL,
  `instructor_id` int(10) UNSIGNED NOT NULL,
  `responsible_id` int(10) UNSIGNED NOT NULL,
  `selected_topic_id` int(10) UNSIGNED NOT NULL,
  `topic1_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `topic2_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `topic3_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phonenumber` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `studen_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `specialized` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `school_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `midterm_report` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `endterm_report` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `finished_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `id`, `enterprise_instructor_id`, `instructor_id`, `responsible_id`, `selected_topic_id`, `topic1_id`, `topic2_id`, `topic3_id`, `fullname`, `phonenumber`, `class_name`, `studen_code`, `address`, `specialized`, `school_name`, `state`, `midterm_report`, `endterm_report`, `finished_date`, `created_at`, `updated_at`) VALUES
(1, 4, 0, 0, 0, 0, '1', '2', '', 'Son Nguyen Ngoc', '', 'inpg12', '20138377', '', '', '', '', '', '', '0000-00-00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phonenumber` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('instructor','reponsible') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`teacher_id`, `id`, `fullname`, `phonenumber`, `address`, `type`, `created_at`, `updated_at`) VALUES
(1, 5, 'Nguyễn Đức Ngọc', '016958584789', 'yen mac , yen mo , ninh binh, viet nam, so 15A,ngo 2 tran quy kien , cau giay , ha noi', 'instructor', NULL, NULL),
(2, 6, 'Phạm Thanh Thịnh', '016958584789', 'hanoi, ninh binh', 'reponsible', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `topic_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `student_number` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `c` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `java` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `android` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `php` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `ios` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `c_w` int(11) NOT NULL,
  `java_w` int(11) NOT NULL,
  `android_w` int(11) NOT NULL,
  `php_w` int(11) NOT NULL,
  `ios_w` int(11) NOT NULL,
  `w` double NOT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`topic_id`, `name`, `student_number`, `instructor_id`, `c`, `java`, `android`, `php`, `ios`, `c_w`, `java_w`, `android_w`, `php_w`, `ios_w`, `w`, `start_time`, `end_time`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Big data1', 20, 3, '2', '3', '3', '4', '1', 4, 2, 1, 1, 1, 2.4, '2017-05-04', '2017-06-04', 'g', NULL, NULL),
(2, 'Big data2', 25, 3, '4', '3', '2', '4', '4', 4, 6, 2, 1, 1, 3.3, '2017-05-04', '2017-06-04', 'fg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'ym1234567891@gmail.com', '$2y$10$QO5MO7ZmhofnaNIVQP7zE.WwBeXWXcPUYg4oozmIdx64aiWCgEO7O', 1, NULL, '2017-05-18 00:13:55', '2017-05-18 00:13:55'),
(2, 'NetBit solution', 'netbit@gmail.com', '$2y$10$AUZCw4c9FjQeyeGN58czDuEHvcFD898fuZiUHWWoe3yOVHxYB6NfW', 4, NULL, '2017-05-18 00:14:13', '2017-05-18 00:14:13'),
(3, 'Nguyễn Ngọc Phương', 'nguyenngocphuong@gmail.com', '$2y$10$7t0KHK.k2TNWn0lfjdxll.ZtEGr7v9PsjPjB7PPeuorb67vKrHP8.', 5, NULL, '2017-05-18 00:14:30', '2017-05-18 00:14:30'),
(4, 'Son Nguyen Ngoc', 'software9113@gmail.com', '$2y$10$OSlqjsjLZ4yLaFLyIS1BIuO4Dm7vswgma3b2A1G2I.hjU9dKaRB2i', 2, NULL, '2017-05-18 00:45:18', '2017-05-18 00:45:18'),
(5, 'Nguyễn Đức Ngọc', 'nguyenducngoc@gmail.com', '$2y$10$YIP7ibiguZC4R2ph1IdqUu6XKMbc2uvlGwY/B0tVovcfOj8zVi8Dy', 3, NULL, '2017-05-18 00:49:07', '2017-05-18 00:49:07'),
(6, 'Phạm Thanh Thịnh', 'p2tpro9z@gmail.com', '$2y$10$5lWrnBuKAhOf2qOK/viwd.xKKjKds118DDlcMrPSViPJUZkOzMamy', 3, NULL, '2017-05-18 00:50:00', '2017-05-18 00:50:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`cv_id`);

--
-- Indexes for table `enterprises`
--
ALTER TABLE `enterprises`
  ADD PRIMARY KEY (`enterprise_id`),
  ADD UNIQUE KEY `enterprises_name_unique` (`name`);

--
-- Indexes for table `instructors`
--
ALTER TABLE `instructors`
  ADD PRIMARY KEY (`instructor_id`),
  ADD KEY `instructors_id_foreign` (`id`),
  ADD KEY `instructors_enterprise_id_foreign` (`enterprise_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `students_id_foreign` (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`teacher_id`),
  ADD KEY `teachers_id_foreign` (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`),
  ADD UNIQUE KEY `topics_name_unique` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cv`
--
ALTER TABLE `cv`
  MODIFY `cv_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `enterprises`
--
ALTER TABLE `enterprises`
  MODIFY `enterprise_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `instructors`
--
ALTER TABLE `instructors`
  MODIFY `instructor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `teacher_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `topic_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `instructors`
--
ALTER TABLE `instructors`
  ADD CONSTRAINT `instructors_enterprise_id_foreign` FOREIGN KEY (`enterprise_id`) REFERENCES `enterprises` (`enterprise_id`),
  ADD CONSTRAINT `instructors_id_foreign` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_id_foreign` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_id_foreign` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
