<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="public/AdminLTE/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="public/AdminLTE/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="public/AdminLTE/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/iCheck/all.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="public/AdminLTE/dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        .color-palette {
            height: 35px;
            line-height: 35px;
            text-align: center;
        }
        
        .color-palette-set {
            margin-bottom: 15px;
        }
        
        .color-palette span {
            display: none;
            font-size: 12px;
        }
        
        .color-palette:hover span {
            display: block;
        }
        
        .color-palette-box h4 {
            position: absolute;
            top: 100%;
            left: 25px;
            margin-top: -40px;
            color: rgba(255, 255, 255, 0.8);
            font-size: 12px;
            display: block;
            z-index: 7;
        }
    </style>
</head>

<body class="skin-blue sidebar-mini">
    <div class="wrapper" style="height: auto;">
        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Admin</b>LTE</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success">4</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 4 messages</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                                        <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                            <li>
                                                <!-- start message -->
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" style="height: 50px">
                                                    </div>
                                                    <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <!-- end message -->
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                AdminLTE Design Team
                                                <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                            </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                Developers
                                                <small><i class="fa fa-clock-o"></i> Today</small>
                                            </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                Sales Department
                                                <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                            </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                Reviewers
                                                <small><i class="fa fa-clock-o"></i> 2 days</small>
                                            </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                        <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                                        <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-red"></i> 5 new members joined
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user text-red"></i> You changed your username
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                        <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </li>
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag-o"></i>
                                <span class="label label-danger">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 9 tasks</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                                        <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                            <li>
                                                <!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                Design some buttons
                                                <small class="pull-right">20%</small>
                                            </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <!-- end task item -->
                                            <li>
                                                <!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                Create a nice theme
                                                <small class="pull-right">40%</small>
                                            </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <!-- end task item -->
                                            <li>
                                                <!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                Some task I need to do
                                                <small class="pull-right">60%</small>
                                            </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">60% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <!-- end task item -->
                                            <li>
                                                <!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                Make beautiful transitions
                                                <small class="pull-right">80%</small>
                                            </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">80% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <!-- end task item -->
                                        </ul>
                                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                        <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </li>
                                <li class="footer">
                                    <a href="#">View all tasks</a>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs">Alexander Pierce</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        Alexander Pierce - Web Developer
                                        <small>Member since Nov. 2012</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar" style="height: auto;">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" style="height: 50px">
                    </div>
                    <div class="pull-left info">
                        <p>Alexander Pierce</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                        </span>
                    </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                            <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                        </ul>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper" style="min-height: 916px;">
            <div class="row">
                <div class="col-md-10">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">Personal information</a></li>
                            <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Topics</a></li>
                            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Add new Topic</a></li>
                            <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Mail & notification</a></li>
                            <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Student Manager</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                              Dropdown <span class="caret"></span>
                              </a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                </ul>
                            </li>
                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_1">
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                    </div>
                                    <!-- /.box-header -->
                                    <!-- form start -->
                                    <form class="form-horizontal" action="instructorsinfo" method="post">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="name1" class="col-sm-2 control-label">Name</label>
                                                <div class="col-sm-10">
                                                    
                                                    <input name="name1" type="text" class="form-control" id="name" placeholder="Name" value="<?php if(isset($users1[0]))echo $users1[0]->name;?>">
                                                   
                                                    
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input name="email1" type="email" class="form-control" id="inputEmail3" placeholder="Email" value="<?php if(isset($users1[0]))echo $users1[0]->email;?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="phonenumber1" class="col-sm-2 control-label">Phonenumber</label>
                                                <div class="col-sm-10">
                                                    <input name="phonenumber1"type="text" class="form-control" id="phonenumber1" placeholder="Phone Number" value="<?php if(isset($instructors[0]))echo $instructors[0]->phonenumber;?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="address1" class="col-sm-2 control-label">Address</label>
                                                <div class="col-sm-10">
                                                    <input name="address1"type="text" class="form-control" id="address1" placeholder="Address" value="<?php if(isset($instructors[0]))echo $instructors[0]->address;?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="company" class="col-sm-2 control-label">Company</label>
                                                <div class="col-sm-10">
                                                    <input name="company"type="text" class="form-control" id="company" placeholder="Company" value="<?php if(isset($enterprises[0]))echo $enterprises[0]->name;?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                                <div class="col-sm-10">
                                                    <input name="password1"type="password" class="form-control" id="inputPassword3" placeholder="Password" value="<?php if(isset($users1[0]))echo $users1[0]->password;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            {!! csrf_field() !!}
                                            <button name="form1" type="submit" class="btn btn-info pull-right" >Save</button>
                                        </div>
                                    </form>
                                    <!-- /.box-body -->
                                    
                                    <!-- /.box-footer -->
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane active" id="tab_2">
                                <div class="box box-info">
                                    <div class="box-header">
                                        <div class="box-tools">
                                            <div class="input-group input-group-sm" style="width: 150px;">
                                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                                                <div class="input-group-btn">
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Students number</th>
                                                    
                                                    <th>Start_time</th>
                                                    <th>End_time</th>
                                                    <th>Description</th>
                                                    
                                                </tr>
                                                <tr>
                                                    @for ($i = 0; $i < count($topics); $i++)
                                                        <tr>
                                                            <td> {{ $topics[$i]->name }}</td>
                                                            <td> {{$sum_student[$topics[$i]->topic_id]}}/{{ $topics[$i]->student_number }}</td>
                                                            
                                                            <td> {{ $topics[$i]->start_time }}</td>
                                                            <td> {{ $topics[$i]->end_time }}</td>
                                                            <td> {{ $topics[$i]->description }}</td>
                                                            <td>
                                                            <a href="?id=<?php echo $topics[$i]->topic_id; ?>">
                                                                <button type="button" class="btn btn-default">Edit</button>
                                                            </a>
                                                            </td>
                                                        </tr>
                                                    @endfor
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                    </div>
                                    <!-- /.box-header -->
                                    <!-- form start -->
                                    <form class="form-horizontal" action="instructorsinfo" method="post">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="nametopic" class="col-sm-2 control-label">Topic</label>
                                                <div class="col-sm-10">
                                                    
                                                    <input name="name_topic" type="text" class="form-control" id="nametopic" placeholder="Topic Name" >
                                                   
                                                    
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="student_number" class="col-sm-2 control-label">Student Number</label>
                                                <div class="col-sm-10">
                                                    <input name="student_number" type="text" class="form-control" id="student_number" placeholder="Student Number" >
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <label for="requirements" class="col-sm-2 control-label">Weight</label>
                                                <div class="col-sm-10">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="c_w" class="col-sm-3 control-label">c_w</label>
                                                                        <input name="c_w" type="text" class="form-control" id="student_number" placeholder="c_w" >
                                                                    </div>
                                                                    
                                                        </div>

                                                        <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="java_w" class="col-sm-3 control-label">java_w</label>
                                                                        <input name="java_w" type="text" class="form-control" id="java_w" placeholder="java_w" >
                                                                    </div>
                                                                    
                                                        </div>
                                                        <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="android_w" class="col-sm-3 control-label">android_w</label>
                                                                        <input name="android_w" type="text" class="form-control" id="android_w" placeholder="android_w" >
                                                                    </div>
                                                                    
                                                        </div>
                                                        <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="php_w" class="col-sm-3 control-label">php_w</label>
                                                                        <input name="php_w" type="text" class="form-control" id="php_w" placeholder="php_w" >
                                                                    </div>
                                                                    
                                                        </div>
                                                        <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="php_w" class="col-sm-3 control-label">ios_w</label>
                                                                        <input name="ios_w" type="text" class="form-control" id="ios_w" placeholder="ios_w" >
                                                                    </div>
                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Toeic point" class="col-sm-2 control-label">Requirements</label>
                                                <div class="col-sm-10">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                <label for="Toeic point" class="col-sm-3 control-label">C/C++</label>
                                                                <select class="form-control" name="c">
                                                                    <option value="1"  >Usability level</option>
                                                                    <option value="2"   >Know</option>
                                                                    <option value="3"   >Can use</option>
                                                                    <option value="4"  >Competently</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                <label for="Toeic point" class="col-sm-3 control-label">Java</label>
                                                                <select class="form-control" name="java">
                                                                    <option value="1"  >Usability level</option>
                                                                    <option value="2"  >Know</option>
                                                                    <option value="3"  >Can use</option>
                                                                    <option value="4"  >Competently</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                <label for="Toeic point" class="col-sm-3 control-label">Java(Android)</label>
                                                                <select class="form-control" name="android">
                                                                    <option value="1"  >Usability level</option>
                                                                    <option value="2"   >Know</option>
                                                                    <option value="3"   >Can use</option>
                                                                    <option value="4"  >Competently</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                <label for="Toeic point" class="col-sm-3 control-label">PHP</label>
                                                                <select class="form-control" name="php">
                                                                    <option value="1"  >Usability level</option>
                                                                    <option value="2"  >Know</option>
                                                                    <option value="3"  >Can use</option>
                                                                    <option value="4"  >Competently</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                <label for="Toeic point" class="col-sm-3 control-label">C/(IOS)</label>
                                                                <select class="form-control" name="ios">
                                                                    <option value="1" >Usability level</option>
                                                                    <option value="2"  >Know</option>
                                                                    <option value="3"  >Can use</option>
                                                                    <option value="4" >Competently</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="start_time" class="col-sm-2 control-label">Start Time</label>
                                                <div class="col-sm-10">
                                                    <div class="form-group">
                                                        

                                                        <div class="input-group date">
                                                          <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                          </div>
                                                          <input name="start_time" type="text" class="form-control pull-right" id="datepicker">
                                                        </div>
                                                        <!-- /.input group -->
                                                      </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="end_time" class="col-sm-2 control-label">End Time</label>
                                                <div class="col-sm-10">
                                                    <div class="form-group">
                                                        

                                                        <div class="input-group date">
                                                          <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                          </div>
                                                          <input name="end_time" type="text" class="form-control pull-right" id="datepicker">
                                                        </div>
                                                        <!-- /.input group -->
                                                      </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                              <label for="description" class="col-sm-2 control-label">Description</label>
                                                <div class="col-sm-10">
                                                
                                                    <textarea id ="description"name="description" class="form-control" rows="3" placeholder="description ...">
                                                    
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            {!! csrf_field() !!}
                                            <button name="save" type="submit" class="btn btn-info pull-right" >Save</button>
                                        </div>
                                    </form>
                                    <!-- /.box-body -->
                                    
                                    <!-- /.box-footer -->
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_4">
                                <section class="content" style="background-color:#E6E6FA">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>
                                            <div class="box box-solid">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Folders</h3>
                                                    <div class="box-tools">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="box-body no-padding">
                                                    <ul class="nav nav-pills nav-stacked">
                                                        <li class="active"><a href="#"><i class="fa fa-inbox"></i> Inbox
                                                              <span class="label label-primary pull-right">3</span></a>
                                                        </li>
                                                        <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
                                                        <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>
                                                        <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a>
                                                        </li>
                                                        <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
                                                    </ul>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                            <!-- /. box -->
                                            <!-- /.box -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-9">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Inbox</h3>
                                                    <div class="box-tools pull-right">
                                                        <div class="has-feedback">
                                                            <input type="text" class="form-control input-sm" placeholder="Search Mail">
                                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body no-padding">
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                                                        </button>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                                                        </div>
                                                        <!-- /.btn-group -->
                                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                                                        <div class="pull-right">
                                                            1-50/200
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                                            </div>
                                                            <!-- /.btn-group -->
                                                        </div>
                                                        <!-- /.pull-right -->
                                                    </div>
                                                    <div class="table-responsive mailbox-messages">
                                                        <table class="table table-hover table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
                                                                            <input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </td>
                                                                    <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                                                                    <td class="mailbox-name"><a href="read-mail.html">Admin</a></td>
                                                                    <td class="mailbox-subject"><b>Submit a Report</b> - Please submit your report before the date ...
                                                                    </td>
                                                                    <td class="mailbox-attachment"></td>
                                                                    <td class="mailbox-date">5 mins ago</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
                                                                            <input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </td>
                                                                    <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                                                                    <td class="mailbox-name"><a href="read-mail.html">Nguyễn Thị Hòa</a></td>
                                                                    <td class="mailbox-subject"><b>Transcripts</b> - Trying to find a solution to this problem...
                                                                    </td>
                                                                    <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                                                                    <td class="mailbox-date">28 mins ago</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
                                                                            <input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </td>
                                                                    <td class="mailbox-star"><a href="#"><i class="fa fa-star-o text-yellow"></i></a></td>
                                                                    <td class="mailbox-name"><a href="read-mail.html">Admin</a></td>
                                                                    <td class="mailbox-subject"><b>Personal informatione</b> - Please fill out all personal information...
                                                                    </td>
                                                                    <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                                                                    <td class="mailbox-date">11 hours ago</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- /.table -->
                                                    </div>
                                                    <!-- /.mail-box-messages -->
                                                </div>
                                                <!-- /.box-body -->
                                                <div class="box-footer no-padding">
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                                                        </button>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                                                        </div>
                                                        <!-- /.btn-group -->
                                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                                                        <div class="pull-right">
                                                            1-50/200
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                                            </div>
                                                            <!-- /.btn-group -->
                                                        </div>
                                                        <!-- /.pull-right -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /. box -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </section>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_5">
                                <div class="box box-info">
                                    <div class="box-header">
                                        <div class="box-tools">
                                            <div class="input-group input-group-sm" style="width: 150px;">
                                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                                                <div class="input-group-btn">
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Students ID</th>
                                                        <th>class</th>
                                                        <th>Timesheet</th>
                                                        <th>Report</th>
                                                        
                                                        
                                                    </tr>
                                                    <tr>
                                                        @for ($i = 0; $i < count($student_mng); $i++)
                                                            <tr>
                                                                <td> {{ $student_mng[$i]->fullname }}</td>
                                                                <td> {{$student_mng[$i]->studen_code }}</td>
                                                                
                                                                <td> {{ $student_mng[$i]->class_name }}</td>
                                                                <td> 
                                                                    <?php 
                                                                        echo "<input type=\"file\" name=\"timesheet[".$student_mng[$i]->studen_code."]\">";
                                                                        if($student_mng[$i]->timesheet==null){
                                                                            //echo "<input type=\"file\" name=\"timesheet[".$student_mng[$i]->studen_code."]\"";
                                                                        }
                                                                        else echo "<a href=\"/quanlythuctap/public/fileupload/upload/".$student_mng[$i]->studen_code."_timesheet.".$student_mng[$i]->timesheet."\">download</a>";
                                                                    ?>
                                                                </td>
                                                                <td> 
                                                                    <?php 
                                                                        echo "<input type=\"file\" name=\"report[".$student_mng[$i]->studen_code."]\">";
                                                                        if($student_mng[$i]->report_company==null){
                                                                            //echo "<input type=\"file\" name=\"report[".$student_mng[$i]->studen_code."]\"";
                                                                        }
                                                                        else echo "<a href=\"/quanlythuctap/public/fileupload/upload/".$student_mng[$i]->studen_code."_report.".$student_mng[$i]->report_company."\">download</a>";
                                                                    ?>
                                                                </td>
                                                                
                                                            </tr>
                                                        @endfor
                                                        
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                                {!! csrf_field() !!}
                                                <button name="save1" type="submit" class="btn btn-info pull-right" >Save</button>
                                            
                                        </form>
                                            
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
                <script src="public/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
                <script src="public/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
                <!-- iCheck -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
                <script src="public/AdminLTE/plugins/iCheck/icheck.min.js"></script>
                <script src="public/AdminLTE/plugins/select2/select2.full.min.js"></script>
                <script src="public/AdminLTE/plugins/daterangepicker/daterangepicker.js"></script>
                <script src="public/AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
                <script src="public/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
                <script src="public/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js"></script>
                <script src="public/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <script src="public/AdminLTE/plugins/iCheck/icheck.min.js"></script>
                <script src="public/AdminLTE/plugins/fastclick/fastclick.js"></script>
                <script src="public/AdminLTE/dist/js/app.min.js"></script>
                <script src="public/AdminLTE/dist/js/demo.js"></script>
                <script>
                    $(function() {
                        $('input').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</body>