<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="public/AdminLTE/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="public/AdminLTE/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="public/AdminLTE/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/iCheck/all.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="public/AdminLTE/dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        .color-palette {
            height: 35px;
            line-height: 35px;
            text-align: center;
        }
        
        .color-palette-set {
            margin-bottom: 15px;
        }
        
        .color-palette span {
            display: none;
            font-size: 12px;
        }
        
        .color-palette:hover span {
            display: block;
        }
        
        .color-palette-box h4 {
            position: absolute;
            top: 100%;
            left: 25px;
            margin-top: -40px;
            color: rgba(255, 255, 255, 0.8);
            font-size: 12px;
            display: block;
            z-index: 7;
        }
    </style>
</head>
<body>
    <div class="box">
        <div class="box-header">
          <h3 class="box-title">aaaaaaaaaa</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <form class="form-horizontal" method="post" action="">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Student Name</th>
                  <th>Class</th>
                  <th>Student ID</th>
                  <th>Progress</th>
                  <th>%</th>
                  <th>Weight</th>
                  <th style="width: 40px">verify</th>
                  
                  
                  <th>Instructor</th>
                  
                </tr>
                
                    @for ($i = 0; $i < count($students); $i++)
                                                           
                        <tr>
                          <td>{{$i+1}}</td>
                          <td>{{$students[$i]->fullname}}</td>
                          <td>{{$students[$i]->class_name}}</td>
                          <td>{{$students[$i]->studen_code}}</td>
                          <td>
                            <div class="progress progress-xs progress-striped active">
                              <div class="progress-bar progress-bar-success" style="width: <?php 
                                if((100*$student_w[$students[$i]->id]/$topic_edit[0]->w)>=100) echo"100%";
                                else echo round(100*$student_w[$students[$i]->id]/$topic_edit[0]->w,1).'%'
                              ?>"></div>
                            </div>
                          </td>
                          <td><span class="badge bg-green"><?php 
                                if((100*$student_w[$students[$i]->id]/$topic_edit[0]->w)>=100) echo"100%";
                                else echo round(100*$student_w[$students[$i]->id]/$topic_edit[0]->w,1).'%'
                              ?>
                          </span></td>
                          <td>{{$student_w[$students[$i]->id]}}/{{$topic_edit[0]->w}}</td>
                          <td>
                            
                                <div class="col-sm-2">
                                    
                                    <input name="students_id[]" type="checkbox" value="{{$students[$i]->id}}"class="minimal" >    
                                </div>
                            
                          </td>
                          
                          <td>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        
                                        <select class="form-control" name="instructor[]">
                                        <?php 
                                            for($j=0;$j<count($instructors);$j++)
                                            echo "<option value=\"".$instructors[$j]->id."\"  >".$instructors[$j]->fullname."</option>";
                                        ?>
                                            
                                            
                                        </select>
                                    </div>
                                </div>
                          </td>
                        </tr>
                    @endfor
                    
              </tbody>
              </table>
                </div>
                {!! csrf_field() !!}
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right" name="savetopic">Save</button>
                    
                </div>
            </form>
        <!-- /.box-body -->
      </div>
    <script src="public/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
    <script src="public/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="public/AdminLTE/plugins/iCheck/icheck.min.js"></script>
    <script src="public/AdminLTE/plugins/select2/select2.full.min.js"></script>
    <script src="public/AdminLTE/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="public/AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="public/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="public/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="public/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="public/AdminLTE/plugins/iCheck/icheck.min.js"></script>
    <script src="public/AdminLTE/plugins/fastclick/fastclick.js"></script>
    <script src="public/AdminLTE/dist/js/app.min.js"></script>
    <script src="public/AdminLTE/dist/js/demo.js"></script>
    <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>