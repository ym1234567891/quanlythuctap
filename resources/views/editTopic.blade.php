<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="public/AdminLTE/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="public/AdminLTE/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="public/AdminLTE/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/iCheck/all.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="public/AdminLTE/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="public/AdminLTE/dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        .color-palette {
            height: 35px;
            line-height: 35px;
            text-align: center;
        }
        
        .color-palette-set {
            margin-bottom: 15px;
        }
        
        .color-palette span {
            display: none;
            font-size: 12px;
        }
        
        .color-palette:hover span {
            display: block;
        }
        
        .color-palette-box h4 {
            position: absolute;
            top: 100%;
            left: 25px;
            margin-top: -40px;
            color: rgba(255, 255, 255, 0.8);
            font-size: 12px;
            display: block;
            z-index: 7;
        }
    </style>
</head>
<body>
    <div class="box box-info">
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="instructorsinfo" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label for="nametopic" class="col-sm-2 control-label">Topic</label>
                    <div class="col-sm-10">
                        
                        <input name="name_topic" type="text" class="form-control" id="nametopic" placeholder="Topic Name" value="<?php if(isset($topic_edit[0]))echo $topic_edit[0]->name;?>">
                       
                        
                        
                    </div>
                </div>
                <div class="form-group">
                    <label for="student_number" class="col-sm-2 control-label">Student Number</label>
                    <div class="col-sm-10">
                        <input name="student_number" type="text" class="form-control" id="student_number" placeholder="Student Number" value="<?php if(isset($topic_edit[0]))echo $topic_edit[0]->student_number;?>" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="requirements" class="col-sm-2 control-label">Weight</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="c_w" class="col-sm-3 control-label">c_w</label>
                                            <input name="c_w" type="text" class="form-control" id="student_number" placeholder="c_w"  value="<?php if(isset($topic_edit[0]->c_w)) echo$topic_edit[0]->c_w ; ?>">
                                        </div>
                                        
                            </div>

                            <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="java_w" class="col-sm-3 control-label">java_w</label>
                                            <input name="java_w" type="text" class="form-control" id="java_w" placeholder="java_w" value="<?php if(isset($topic_edit[0]->java_w)) echo$topic_edit[0]->java_w ; ?>" >
                                        </div>
                                        
                            </div>
                            <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="android_w" class="col-sm-3 control-label">android_w</label>
                                            <input name="android_w" type="text" class="form-control" id="android_w" placeholder="android_w" value="<?php if(isset($topic_edit[0]->android_w)) echo$topic_edit[0]->android_w ; ?>" >
                                        </div>
                                        
                            </div>
                            <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="php_w" class="col-sm-3 control-label">php_w</label>
                                            <input name="php_w" type="text" class="form-control" id="php_w" placeholder="php_w" value="<?php if(isset($topic_edit[0]->php_w)) echo$topic_edit[0]->php_w ; ?>">
                                        </div>
                                        
                            </div>
                            <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="php_w" class="col-sm-3 control-label">ios_w</label>
                                            <input name="ios_w" type="text" class="form-control" id="ios_w" placeholder="ios_w" value="<?php if(isset($topic_edit[0]->ios_w)) echo$topic_edit[0]->ios_w ; ?>" >
                                        </div>
                                        
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Toeic point" class="col-sm-2 control-label">Requirements</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="Toeic point" class="col-sm-3 control-label">C/C++</label>
                                    <select class="form-control" name="c">
                                        <option value="1" <?php if(($topic_edit[0]->c)==1) echo "selected"; ?> >Usability level</option>
                                        <option value="2" <?php if(($topic_edit[0]->c)==2) echo "selected"; ?>  >Know</option>
                                        <option value="3" <?php if(($topic_edit[0]->c)==3) echo "selected"; ?>  >Can use</option>
                                        <option value="4" <?php if(($topic_edit[0]->c)==4) echo "selected"; ?> >Competently</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="Toeic point" class="col-sm-3 control-label">Java</label>
                                    <select class="form-control" name="java">
                                        <option value="1" <?php if(($topic_edit[0]->java)==1) echo "selected"; ?> >Usability level</option>
                                        <option value="2" <?php if(($topic_edit[0]->java)==2) echo "selected"; ?>  >Know</option>
                                        <option value="3" <?php if(($topic_edit[0]->java)==3) echo "selected"; ?>  >Can use</option>
                                        <option value="4" <?php if(($topic_edit[0]->java)==4) echo "selected"; ?> >Competently</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="Toeic point" class="col-sm-3 control-label">Java(Android)</label>
                                    <select class="form-control" name="android">
                                        <option value="1" <?php if(($topic_edit[0]->android)==1) echo "selected"; ?> >Usability level</option>
                                        <option value="2" <?php if(($topic_edit[0]->android)==2) echo "selected"; ?>  >Know</option>
                                        <option value="3" <?php if(($topic_edit[0]->android)==3) echo "selected"; ?>  >Can use</option>
                                        <option value="4" <?php if(($topic_edit[0]->android)==4) echo "selected"; ?> >Competently</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="Toeic point" class="col-sm-3 control-label">PHP</label>
                                    <select class="form-control" name="php">
                                        <option value="1" <?php if(($topic_edit[0]->php)==1) echo "selected"; ?> >Usability level</option>
                                        <option value="2" <?php if(($topic_edit[0]->php)==2) echo "selected"; ?>  >Know</option>
                                        <option value="3" <?php if(($topic_edit[0]->php)==3) echo "selected"; ?>  >Can use</option>
                                        <option value="4" <?php if(($topic_edit[0]->php)==4) echo "selected"; ?> >Competently</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="Toeic point" class="col-sm-3 control-label">C/(IOS)</label>
                                    <select class="form-control" name="ios">
                                        <option value="1" <?php if(($topic_edit[0]->ios)==1) echo "selected"; ?> >Usability level</option>
                                        <option value="2" <?php if(($topic_edit[0]->ios)==2) echo "selected"; ?>  >Know</option>
                                        <option value="3" <?php if(($topic_edit[0]->ios)==3) echo "selected"; ?>  >Can use</option>
                                        <option value="4" <?php if(($topic_edit[0]->ios)==4) echo "selected"; ?> >Competently</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="start_time" class="col-sm-2 control-label">Start Time</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            

                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input name="start_time" type="text" class="form-control pull-right" id="datepicker" value="<?php if(isset($topic_edit[0]))echo $topic_edit[0]->start_time;?>">
                            </div>
                            <!-- /.input group -->
                          </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="end_time" class="col-sm-2 control-label">End Time</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            

                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input name="end_time" type="text" class="form-control pull-right" id="datepicker" value="<?php if(isset($topic_edit[0]))echo $topic_edit[0]->end_time;?>">
                            </div>
                            <!-- /.input group -->
                          </div>
                    </div>
                </div>

                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                    
                        <textarea id ="description"name="description" class="form-control" rows="3" placeholder="description ...">
                        <?php if(isset($topic_edit[0]))echo $topic_edit[0]->description;?>
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                {!! csrf_field() !!}
                <button name="edit" type="submit" class="btn btn-info pull-right" >edit</button>
            </div>
        </form>
        <!-- /.box-body -->
        
        <!-- /.box-footer -->
    </div>
    <script src="public/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
    <script src="public/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="public/AdminLTE/plugins/iCheck/icheck.min.js"></script>
    <script src="public/AdminLTE/plugins/select2/select2.full.min.js"></script>
    <script src="public/AdminLTE/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="public/AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="public/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="public/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="public/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="public/AdminLTE/plugins/iCheck/icheck.min.js"></script>
    <script src="public/AdminLTE/plugins/fastclick/fastclick.js"></script>
    <script src="public/AdminLTE/dist/js/app.min.js"></script>
    <script src="public/AdminLTE/dist/js/demo.js"></script>
    <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>